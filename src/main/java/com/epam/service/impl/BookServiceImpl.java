package com.epam.service.impl;

import com.epam.dao.interfaces.BookDAO;
import com.epam.entity.Book;
import com.epam.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookDAO bookDAO;

    @Override
    public List<Book> findAll(String langSelect) {
        List<Book> books;
        if (langSelect.equals("en")) {
            books = bookDAO.findALL();

        } else {
            books = bookDAO.findALL(langSelect);
        }
        return books;
    }

    @Override
    public List<Book> findAllUser(Long id, String langSelection) {
        List<Book> books;
        if (langSelection.equals("en")) {
            books = bookDAO.findAllByUser(id);

        } else {
            books = bookDAO.findAllByUser(id, langSelection);
        }
        return books;
    }
}
