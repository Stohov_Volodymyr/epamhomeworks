package com.epam.service;

import com.epam.entity.Book;

import java.util.List;

public interface BookService {
    List<Book> findAll(String langSelect);

    List<Book> findAllUser(Long id, String langSelection);
}
