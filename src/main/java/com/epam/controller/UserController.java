package com.epam.controller;

import com.epam.dao.interfaces.UserDAO;
import com.epam.entity.Book;
import com.epam.entity.User;
import com.epam.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@RequestMapping("/user")
@Controller
@AllArgsConstructor
public class UserController extends HttpServlet {
     private final BookService bookService;
        private final UserDAO userDAOImpl;

    @GetMapping
    public String getUser(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        User userId = userDAOImpl.findById(Long.valueOf(String.valueOf(session.getAttribute("userId")))).orElseThrow(RuntimeException::new);
        req.setAttribute("user", userId);
        String str = String.valueOf(session.getAttribute("lang"));
        List<Book> bookLists = bookService.findAllUser(Long.valueOf(String.valueOf(session.getAttribute("userId"))), str);
        req.setAttribute("bookList", bookLists);
        //считает цену всех книг

        Map<Book, Long> booksToCount = bookLists.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        req.setAttribute("booksToCount", booksToCount);
        req.setAttribute("totalPrice", bookLists.stream().map(Book::getCost).reduce(BigDecimal.ZERO, BigDecimal::add));

        return "user";
    }

}
