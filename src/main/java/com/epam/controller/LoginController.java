package com.epam.controller;

import com.epam.dao.interfaces.UserDAO;
import com.epam.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RequestMapping("/login")
@Controller
@AllArgsConstructor
public class LoginController extends HttpServlet {

    private final UserDAO userDAO;

    @GetMapping
    public String getLogin() {
        return "login";
    }

    @PostMapping
    public String postLogin(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        User user = userDAO.findByLogin(login).orElse(null);
        if (user != null && user.getLogin().equals(login) && user.getPassword().equals(password) && !user.getIsBanned()) {
            HttpSession session = req.getSession();
            session.setAttribute("user", user);
            session.setAttribute("userCash", user.getCash());
            session.setAttribute("userId", user.getId());
            session.setAttribute("login", login);
            session.setAttribute("password", password);
            session.setAttribute("isLogged", user);
            session.setAttribute("userRole", user.getRole());
            session.setAttribute("lang", "en");
            if (user.getRole().getRole().equals("admin")) {
                session.setAttribute("isAdmin", true);
            }
            return "redirect:/about";
        } else if (user != null && user.getIsBanned()) {
            req.setAttribute("ban", true);
            return  "login";
        } else {
            req.setAttribute("pass", true);
            return  "login";
        }
    }
}