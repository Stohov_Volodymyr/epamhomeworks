package com.epam.controller;


import com.epam.dao.interfaces.UserDAO;
import com.epam.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RequestMapping("/userList")
@Controller
@AllArgsConstructor
public class UserListController extends HttpServlet {
    private final UserDAO userDAO;

    @GetMapping
    public String getList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> userList = userDAO.findALL();
        req.setAttribute("userList", userList);
        return "userList";
    }

    @PostMapping
    public String postList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long userId = Long.valueOf(req.getParameter("userId"));
        Boolean isBanned = Boolean.valueOf(req.getParameter("isBanned"));
        userDAO.updateStatus(userId, !isBanned);
        return "redirect:/userList";
    }

}
