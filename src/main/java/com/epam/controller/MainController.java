package com.epam.controller;

import com.epam.entity.Book;
import com.epam.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RequestMapping("/main")
@Controller
@AllArgsConstructor
public class MainController extends HttpServlet {
    private final BookService bookService;

    @GetMapping
    public String getMain(HttpServletRequest req) {
        HttpSession session = req.getSession();
        String str = String.valueOf(session.getAttribute("lang"));
        List<Book> bookLists = bookService.findAll(str);
        req.setAttribute("bookList", bookLists);
        return "main";
    }

    @PostMapping
    public String postMain(HttpServletRequest req) {
        HttpSession session = req.getSession();
        System.out.println(req.getParameter("lang"));
        session.setAttribute("lang", req.getParameter("lang"));
        return "about";
    }

}