package com.epam.controller;

import com.epam.exceptions.LoginException;
import com.epam.exceptions.UserExistException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandlerController {

    @ExceptionHandler(UserExistException.class)
    public final String handleRuntimeException(UserExistException ex){
        return "errorpage/UserExist";
    }

    @ExceptionHandler(LoginException.class)
    public final String handleLoginException(LoginException ex){
        return "errorpage/loginException";
    }

}