package com.epam.controller;

import com.epam.dao.impl.OrderDAOImpl;
import com.epam.dao.impl.UserDAOImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

@RequestMapping("/order")
@Controller
@AllArgsConstructor
public class OrderController {
    private final OrderDAOImpl orderDAOImpl;
    private final UserDAOImpl userDAOImpl;

    @PostMapping
    public String postOrder(HttpServletRequest req) {
        HttpSession session = req.getSession();
        Long userId = Long.valueOf(String.valueOf(session.getAttribute("userId")));
        Long bookId = Long.valueOf(req.getParameter("book_id"));
        BigDecimal bookCost = new BigDecimal(String.valueOf(req.getParameter("book_cost")));

        //чтобы деньги в - не ушли
        BigDecimal userCash = new BigDecimal(
                String.valueOf(session.getAttribute("userCash"))
        ).subtract(bookCost);
        if (userCash.compareTo(new BigDecimal("0.00")) < 0) {
            return "redirect:/main";
        }

        orderDAOImpl.orderBook(bookId, userId);
        userDAOImpl.updateCash(userId, bookCost);
        session.setAttribute("userCash", new BigDecimal(
                String.valueOf(session.getAttribute("userCash"))
        ).subtract(bookCost));
        return "redirect:/main";
    }
}