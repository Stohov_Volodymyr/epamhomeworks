package com.epam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(value = "/about")
@Controller
public class AboutController {

    @GetMapping
    public String getAbout() {
        return "about";
    }

}
