package com.epam.controller;

import com.epam.dao.interfaces.UserDAO;
import com.epam.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RequestMapping("/test")
@Controller
@AllArgsConstructor
public class TestController {
    private final UserDAO userDAOImpl;

    @GetMapping
    public String getTest(HttpServletRequest req) throws ServletException, IOException {
        User userLists = userDAOImpl.findById(7L).orElseThrow(RuntimeException::new);
        req.setAttribute("user",userLists);
        return "test.jsp";
    }

}
