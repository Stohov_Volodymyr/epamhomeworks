package com.epam.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RequestMapping("/logout")
@Controller
@AllArgsConstructor
public class LogoutController {

    @GetMapping
    public String getLogout(HttpServletRequest req) throws IOException {
        HttpSession session = req.getSession();

        if(session.getAttribute("login")!=null){
            session.invalidate();
        }
        return "redirect:/login";
    }

}
