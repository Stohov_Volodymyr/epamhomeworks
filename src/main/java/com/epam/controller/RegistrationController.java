package com.epam.controller;

import com.epam.dao.interfaces.UserDAO;
import com.epam.entity.User;
import com.epam.exceptions.UserExistException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequestMapping("/registration")
@Controller
@AllArgsConstructor
public class RegistrationController extends HttpServlet {
    private final UserDAO userDAO;

    @GetMapping
    public String getRegistration() {
        return "registration";
    }

    @PostMapping
    public String postRegistration(HttpServletRequest req, HttpServletResponse resp) {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String email = req.getParameter("email");

        if (login == null || password == null || email == null || login.isEmpty() || password.isEmpty() || email.isEmpty()) {
            req.getRequestDispatcher("meme.jsp");
        }
        if (userDAO.findByLogin(login).isPresent()) {
            throw new UserExistException();
        }
        if (userDAO.findByEmail(email).isPresent()) {
            throw new UserExistException();
        }
        User user = User.builder()
                .login(login)
                .password(password)
                .email(email)
                .role(null)
                .isBanned(false)
                .build();
        userDAO.create(user);
        return "main";
    }
}
