package com.epam.controller;

import com.epam.dao.interfaces.UserDAO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.math.BigDecimal;

@RequestMapping("/magic")
@Controller
@AllArgsConstructor
public class CashController extends HttpServlet {
    private final UserDAO userDAOImpl;

    @PostMapping
    public void postMagic(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession();
        Long userId = Long.valueOf(String.valueOf(session.getAttribute("userId")));
        BigDecimal cash_add = new BigDecimal(String.valueOf(req.getParameter("cash_add")));
        userDAOImpl.updateCash(userId, cash_add);
        session.setAttribute("userCash", new BigDecimal(
                String.valueOf(session.getAttribute("userCash"))
        ).subtract(cash_add));
        resp.sendRedirect("/user");
    }

}
