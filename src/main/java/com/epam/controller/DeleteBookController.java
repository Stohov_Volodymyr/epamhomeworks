package com.epam.controller;

import com.epam.dao.interfaces.BookDAO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


@RequestMapping("/delete")
@Controller
@AllArgsConstructor
public class DeleteBookController extends HttpServlet {
    private final BookDAO bookDAO;

    @PostMapping
    public String postDelete(HttpServletRequest req) {
        String idBook = req.getParameter("book_id");
        Long longIdBooks = Long.valueOf(idBook);
        bookDAO.deleteById(longIdBooks);
        return "redirect:/main";
    }

}
