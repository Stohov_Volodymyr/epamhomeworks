package com.epam.controller;

import com.epam.dao.interfaces.BookDAO;
import com.epam.entity.Book;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

@RequestMapping("/edit")
@Controller
@AllArgsConstructor
public class BookEditController {

    private final BookDAO bookDAO;

    @PostMapping
    public void postBookEdit(HttpServletRequest req,HttpServletResponse resp) throws IOException {
        String cost = req.getParameter("cost");
        BigDecimal bigDecimal = new BigDecimal(cost);
        String idbooks = req.getParameter("editBookId");
        Long longIdBooks = Long.valueOf(idbooks);

        Book book = Book.builder()
                .author(req.getParameter("author"))
                .cost(bigDecimal)
                .idbooks(longIdBooks)
                .title(req.getParameter("title")).build();
        bookDAO.update(book);
        resp.sendRedirect("/main");
    }
}