package com.epam.dao.interfaces;

import com.epam.entity.Book;

import java.util.List;

public interface BookDAO extends GenericDAO<Book, Long> {
    List<Book> findAllByUser(Long id);
    List<Book> findAllByUser(Long id, String aaa);
    List<Book> findALL(String name);
}
//