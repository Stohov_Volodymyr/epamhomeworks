package com.epam.dao.interfaces;

public interface OrderDAO{

    Boolean orderBook(Long bookId, Long userId);

}
