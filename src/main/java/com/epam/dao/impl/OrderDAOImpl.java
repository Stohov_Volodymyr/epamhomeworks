package com.epam.dao.impl;

import com.epam.dao.interfaces.OrderDAO;
import com.epam.entity.ConnectionPool;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
@Repository
@AllArgsConstructor
public class OrderDAOImpl implements OrderDAO {

    private final Connection connection;


    @Override
    public Boolean orderBook(Long bookId, Long userId) {
        String sql = "insert into user_book (users_id, books_idbooks) values (?,?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setLong(1, userId);
            preparedStatement.setLong(2, bookId);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

}
