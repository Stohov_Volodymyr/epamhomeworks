<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="../directive/lang.jspf" %>
<%@ page isELIgnored="false" %>
<!doctype html>
<html lang="${lang}">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../../resourcesWeb/style.css">

    <style type="text/css">
        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            text-align: left;
            font-size: 20px;
            border-collapse: separate;
            border-spacing: 5px;
            background: #ffffff;
            color: #656665;
            border: 5px solid #ffffff;
        }

        th {
            font-size: 18px;
            padding: 10px;
        }

        td {
            background: #e6e6e6;
            padding: 5px;
            padding-left: 10px;
        }
    </style>

</head>
<body>
<form class="langForm" action="main" method="post">
    <input class="langButton ru" type="submit" name="lang" value="ru">
    <input class="langButton en" type="submit" name="lang" value="en">
</form>
<%@ include file="../directive/header.jspf" %>
</br>
<table border="1" align="center" width="70%">
        <tr>
            <td width="20%">
                <p align="center"><img src="../../resourcesWeb/user.png" width="70%"></p>
            </td>
            <td>
                <p><fmt:message key="user_jsp.login"/>&nbsp<c:out value="${user.getLogin()}"/><p>
                <p><fmt:message key="user_jsp.email"/>&nbsp<c:out value="${user.getEmail()}"/><p>
                <p><fmt:message key="user_jsp.cost"/>&nbsp<c:out value="${totalPrice}"/><fmt:message key="user_jsp.grn"/><p>
                <p><fmt:message key="user_jsp.cash"/><c:out value="${user.getCash()}"/><fmt:message key="user_jsp.grn"/><p>

                <form action="magic" method="post">
                    <input type="hidden" name="cash_add" value ="-100">
                    <input type="submit" value="+100">
                </form></td>

    </table>
</br>
    </td>
    <tr>
        </table>

<c:forEach var="entry" items="${booksToCount}">
            <table border="1" align="center" width="70%">
                <tr>
                    <td width="30%"><c:out value="${entry.key.title}"/></td>
                    <td width="30%"><c:out value="${entry.key.author}"/></td>
                    <td width="15%"><c:out value="${entry.value}"/></td>
                </tr>
            </table>
</c:forEach>

</body>
</html>
