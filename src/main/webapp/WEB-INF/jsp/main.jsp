<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="../directive/lang.jspf" %>
<!doctype html>
<html lang="${lang}">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="../../resourcesWeb/style.css">
    <script src="js.js"></script>
    <style>
        #zatemnenie {
            background: rgba(102, 102, 102, 0.5);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            display: none;
        }

        #okno {
            width: 300px;
            height: 370px;
            text-align: center;
            padding: 15px;
            border: 30px solid #FFDAB9;
            border-radius: 10px;
            color: black;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: auto;
            background: #fff;
        }

        #zatemnenie:target {
            display: block;
        }

        .close {
            display: inline-block;
            border: 1px solid black;
            color: black;
            padding: 0 12px;
            margin: 10px;
            text-decoration: none;
            background: #f2f2f2;
            font-size: 14pt;
            cursor: pointer;
        }

        .close:hover {
            background: #e6e6ff;
        }
    </style>

    <style type="text/css">
        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            text-align: left;
            font-size: 20px;
            border-collapse: separate;
            border-spacing: 5px;
            background: #ffffff;
            color: #656665;
            border: 5px solid #ffffff;
        }

        th {
            font-size: 18px;
            padding: 10px;
        }

        td {
            background: #e6e6e6;
            padding: 5px;
            padding-left: 10px;
        }
    </style>

</head>
<body>
    <%@ include file="../directive/header.jspf" %>

    <form class="langForm" action="main" method="post">
        <input class="langButton ru" type="submit" name="lang" value="ru">
        <input class="langButton en" type="submit" name="lang" value="en">
    </form>
</br>
    <table border="1" align="center" width="90%" id="info-table">
        <thead>
        <tr>
            <th scope="col">Book name</th>
            <th scope="col">Author</th>
            <th scope="col">Cost</th>
            <c:if test="${login != null}">
                <th scope="col">Order</th>
            </c:if>
            <c:if test="${login != null}">
                <th scope="col">Online reading</th>
            </c:if>
            <c:if test="${isAdmin}">
                <th scope="col">Edit</th>
            </c:if>
            <c:if test="${isAdmin}">
                <th scope="col">Delete</th>
            </c:if>
        </tr>
        </thead>


        <c:if test="${login != null}">


        </c:if>
        <c:forEach var="book" items="${bookList}">

            <tr>
                <td width="30%"><c:out value="${book.getTitle()}"/></td>
                <td width="30%"><c:out value="${book.getAuthor()}"/></td>
                <td width="10%"><c:out value="${book.getCost()}"/></td>
                <c:if test="${login != null}">
                    <td width="5%">
                        <form action="order" method="post">
                            <input type="hidden" name="book_id" value="${book.getIdbooks()}">
                            <input class="bookCost${book.getIdbooks()}" type="hidden" name="book_cost"
                                   value="${book.getCost()}">
                            <input type="submit" value="Order">
                        </form>
                    </td>

                    <td>
                        <a href="${book.getLink()}">Read</a>
                    </td>

                </c:if>
                <c:if test="${isAdmin}">
                    <td width="5%">
                        <input class="bookCost${book.getIdbooks()}" type="hidden" name="book_cost"
                               value="${book.getCost()}">
                        <div id="zatemnenie">
                            <div id="okno">

                                <form action="edit" method="post">
                                    <input type="hidden" name="editBookId" class="this_book_edit_id" value="">
                                    <div class="form-group">
                                        <label for="exampleInputTitle">Edit book title</label>
                                        <input class="form-control" name="title" id="exampleInputEmail1"
                                               aria-describedby="emailHelp" placeholder="Enter title">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputAuthor">Edit author</label>
                                        <input class="form-control" name="author" id="exampleInputPassword1"
                                               placeholder="Author">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputAuthor">Edit cost</label>
                                        <input class="form-control" name="cost" id="exampleInputPassword1"
                                               placeholder="Cost">
                                    </div>
                                    <a href="#" class="close">Close</a>
                                    <input class="close" type="submit" value="Submit">
                                </form>

                            </div>
                        </div>
                        <a href="#zatemnenie">
                            <button onclick="edit(${book.getIdbooks()})" value="${book.getIdbooks()}">Edit</button>
                        </a>

                        </form>
                    </td>
                </c:if>

                <c:if test="${isAdmin}">
                    <td width="5%">
                        <form action="delete" method="post">
                            <input type="hidden" name="book_id" value="${book.getIdbooks()}">
                            <input class="bookCost${book.getIdbooks()}" type="hidden" name="book_cost"
                                   value="${book.getCost()}">
                            <input type="submit" value="Delete">
                        </form>
                    </td>
                </c:if>

            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>