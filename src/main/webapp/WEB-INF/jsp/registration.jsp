<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ include file="../directive/lang.jspf" %>
<%@ page isELIgnored="false" %>
<!doctype html>
<html lang="${lang}">
<head>
    <meta charset="UTF-8">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
<meta name="viewport"
content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Document</title>
 <link rel="stylesheet" href="../../resourcesWeb/style.css">

 <style type="text/css">
     table {
         font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
         text-align: left;
         font-size: 20px;
         border-collapse: separate;
         border-spacing: 5px;
         background: #ffffff;
         color: #656665;
         border: 5px solid #ffffff;
     }

     th {
         font-size: 18px;
         padding: 10px;
     }

     td {
         background: #e6e6e6;
         padding: 5px;
         padding-left: 10px;
     }
         </style>

</head>

<body>
<form class="langForm" action="main" method="post">
    <input class="langButton ru" type="submit" name="lang" value="ru">
    <input class="langButton en" type="submit" name="lang" value="en">
</form>

<%@ include file="../directive/header.jspf" %>


<div class="form">
<h1>Registration</h1>
<div class="input-form">
<form action="registration" method="post">
<p style="color:white;">Login:</p><input type="text" name="login"><br>
</div>
<div class="input-form">
<p style="color:white;">Email:</p><input type="email" name="email"></br>
</div>
<div class="input-form">
<p style="color:white;">Password:</p><input type="password" name="password"></br>
</div>
<div class="input-form">
<p style="color:white;"><a style="color:white;" href="/login">Have account? Login</a></p>
<input type="submit" value="registration">
</div>
</form>
</body>


</html>
