<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<%@ include file="../directive/lang.jspf" %>
<!doctype html>
<html lang="${lang}">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/resourcesWeb/style.css">
    <style type="text/css">
        table {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            text-align: left;
            font-size: 20px;
            border-collapse: separate;
            border-spacing: 5px;
            background: #ffffff;
            color: #656665;
            border: 5px solid #ffffff;
        }

        th {
            font-size: 18px;
            padding: 10px;
        }

        td {
            background: #e6e6e6;
            padding: 5px;
            padding-left: 10px;
        }
            </style>

</head>
<body>


<%@ include file="../directive/header.jspf" %>
<form class="langForm" action="main" method="post">
    <input class="langButton ru" type="submit" name="lang" value="ru">
    <input class="langButton en" type="submit" name="lang" value="en">
</form>

<div class="form">
    <h1>Library</h1>
    <div class="input-form">
    <form action="login" method="post">
<p style="color:white;">Login:</p><input type="text" name="login"><br>
</div>
<div class="input-form">
<p style="color:white;">Password:</p><input type="password" name="password"></br>
</div>
<div class="input-form">
<p style="color:white;"><a style="color:white;" href="/registration">Registration</a></p>

<c:if test="${pass}">
        <p style="color:white;">Incorrect login or password</p>
</c:if>

<c:if test="${ban}">
        <p style="color:white;">You are banned</p>
</c:if>

<input type="submit" value="Login">
</div>

<c:out value="${user}"/>

<div></br></div>
</body>
</html>
